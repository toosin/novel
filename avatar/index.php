<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 检测PHP环境
if (version_compare(PHP_VERSION, '5.3.0', '<')) {
    die('require PHP > 5.3.0 !');
}

if( isset( $_SERVER['TP_MODULE'] ) ) {
    $tp_module = strtolower( trim( $_SERVER['TP_MODULE'] ) );
}

$tp_module = isset($tp_module)? $tp_module : 'Admin';

define( 'BIND_MODULE' , $tp_module );


// 定义应用目录
define('APP_PATH', '../Application/');

// 定义web目录
define("WEB_ROOT", dirname(dirname(__FILE__)));

// 找到相应配置APP_STATUS
$curr_app_status = isset( $_COOKIE['hh'] ) ? $_COOKIE['hh'] : "";
if( isset( $_SERVER['TP_ENV'] ) ) {
    $curr_app_status = strtolower( trim( $_SERVER['TP_ENV'] ) );
}
if ($curr_app_status == '') {
    $curr_app_status = 'kangzishangcheng';
}

setcookie('hh', $curr_app_status);
define('APP_STATUS', $curr_app_status);

define("APP_ROOT", dirname(dirname(__FILE__)));

//定义runtime文件夹
if( defined('APP_STATUS') ) {
    define('RUNTIME_PATH', APP_PATH .'Runtime_admin_'.APP_STATUS.'/');
}

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
$debug = isset($_GET['debug']) ? true : false;
define('APP_DEBUG', $debug);

// 引入ThinkPHP入口文件
require WEB_ROOT . '/ThinkPHP/ThinkPHP.php';

// 亲^_^ 后面不需要任何代码了 就是如此简单