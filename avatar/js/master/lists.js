(function(){
	var sAjaxSource = '?m=admin&c=master&a=lists&_a=listsData';
	/* Add the events etc before DataTables hides a column */
	$("#datatable thead input").keyup(function() {
		datatable.fnFilter(this.value, datatable.oApi._fnVisibleToColumnIndex(datatable.fnSettings(), $("thead input").index(this)));
	});

	$("#datatable thead input").each(function(i) {
		this.initVal = this.value;
	});
	$("#datatable thead input").focus(function() {
		if (this.className == "search_init") {
			this.className = "";
			this.value = "";
		}
	});
	$("#datatable thead input").blur(function(i) {
		if (this.value == "") {
			this.className = "search_init";
			this.value = this.initVal;
		}
	});		
	var datatable = $('#datatable').dataTable(
	{
		"sDom" : "<'dt-top-row'>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
		"bSortCellsTop" : true,
		"oTableTools" : 
		{
			"aButtons" : [
				"copy", 
				{
					"sExtends" : "collection",
					"sButtonText" : 'Save <span class="caret" />',
					"aButtons" : ["csv", "xls", "pdf"]
				}
			],
			"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
		},
		"fnInitComplete" : function(oSettings, json)
		{
			$(this)
			.closest('#dt_table_tools_wrapper')
			.find('.DTTT.btn-group')
			.addClass('table_tools_group')
			.children('a.btn')
			.each(function()
				{
					$(this).addClass('btn-sm btn-default');
				}
			);
			this.fnAdjustColumnSizing(true);
		},
	    "fnDrawCallback": function ( oSettings )
	    {
	    	//pageSetUp();
	    },
		"bFilter":true,
		"sEmptyTable": "Empty Data",
		"bServerSide": true,
		'bPaginate': true, //
		"bProcessing": true, //
		"oLanguage": langage,
		"sAjaxSource": sAjaxSource,
		"sAjaxDataProp": "content.data",
		"iDisplayLength": 20,
		"sPaginationType": "bootstrap_full",
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"sScrollX": "100%",
		"sScrollXInner": "100%",
		"bScrollCollapse": false,
		"aoColumns": [{ "mData": "id" ,"bSortable": true, "sWidth": "5em"},
		              { "mData": "name" ,"bSortable": true, "sWidth": "10em"},
		              { "mData": "key" ,"bSortable": true, "sWidth": "15em", "sClass": "mgWordBreak"},
		              { "mData": "note" ,"bSortable": true},
		              { "mData": "state" ,"bSortable": true, "sWidth": "2em"},
		              { "mData": "intime" ,"bSortable": true, "sWidth": "10em"},
		              { "mData": "options" ,"bSortable": false, "sWidth": "5em"}]
	});
})(null);


function add()
{
	$.ajax({
		type	: 'GET',
		url		: '?m=admin&c=master&a=add'
	}).done(function(r){
		$('#detailInfo').html(r);
		$('#detailInfo').dialog({title:'添加新配置', width: 700});
		$('#detailInfo').dialog('open');
	});
}

function edit(id, name)
{
	$.ajax({
		type	: 'GET',
		url		: '?m=admin&c=master&a=edit&id='+id
	}).done(function(r){
		$('#detailInfo').html(r);
		$('#detailInfo').dialog({title:'修改配置【'+name+'】', width: 700});
		$('#detailInfo').dialog('open');
	});
}