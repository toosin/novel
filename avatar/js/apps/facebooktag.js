/**
 * 脸谱标签
 */
//$('#facebook_tags').on("change", function(e) { console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); });
function facebook_tagsSelect2()
{
	$('#facebook_tags').select2({
		id: function(e) { return e.userid; },
	    placeholder          : "Search for a repository",
	    minimumInputLength   : 1,
	    multiple             : true,
	    separator            : ",",                             // 分隔符
	    maximumSelectionSize : 30,                               // 限制数量
	    initSelection        : function (element, callback) {   // 初始化时设置默认值
	        var data = [];
	        $(element.val().split(",")).each(function () {
	            data.push({userid: this, alias: this});
	        });
	        callback(data);
	    },
	    createSearchChoice   : function(term, data) {           // 创建搜索结果（使用户可以输入匹配值以外的其它值）
	        //return { userid: term, alias: term };
	    },
	    formatSelection : function (item) { return item.userid; },  // 选择结果中的显示
	
		formatResult: function(repo)
		{
			var markup = "<div><span class='padding-10'>" + repo.userid + "</span><span class='padding-10'>" + repo.alias + "</span></div>";
			if (repo.name) {
				markup += '<div>' + repo.name + '</div>';
			}
			return markup;
		},  // 搜索列表中的显示
	    ajax : {
	        url      : "?m=admin&c=apps&a=lists&_c=appsData&_a=getFacebookTag",              // 异步请求地址
	        dataType : "json",                  // 数据类型
	        data     : function (term, page) {  // 请求参数（GET）
	        	var limit = '';
				var _app_installed_num = $(':radio[name="k[app_installed_num_limit]"]');
				_app_installed_num.each(function(a,b){
					if($(this).prop('checked'))
					{
						limit = $(this).val();
					}
				});
	            return { query: term ,limit:limit};
	        },
	        results      : function (data, page) { return data.content; },  // 构造返回结果
	        escapeMarkup : function (m) { return m; }               // 字符转义处理
	    }
	});
}
/**
*脸谱标签选择函数
*/
$('#sex_not_limit').click(function(){
	var facebook_tags = $('#facebook_tags').val();
	var sex_male 	  = $('#sex_male').val();
	var sex_female 	  = $('#sex_female').val();
	var ft			  = facebook_tags.split(",");
	var obj = [];
	$(ft).each(function(a,b){
		if(!b)return;
		if(b==sex_male || b==sex_female)
		{
			//ft[a] = ft[a+1];
			//ft.length --;
		}else{
			obj.push({userid: b, alias: b});
		}
	});
	$('#facebook_tags').select2('data',obj);
});
$('#sex_male').click(function(){
	var facebook_tags = $('#facebook_tags').val();
	var sex_male 	  = $('#sex_male').val();
	var sex_female 	  = $('#sex_female').val();
	var ft			  = facebook_tags.split(",");
	var obj = [];
	$(ft).each(function(a,b){
		if(!b)return;
		if(b==sex_male || b==sex_female)
		{
			//ft[a] = ft[a+1];
			//ft.length --;
		}else{
			obj.push({userid: b, alias: b});
		}
	});
	obj.push({userid: sex_male, alias: sex_male});
	
	$('#facebook_tags').select2('data',obj);
});
$('#sex_female').click(function(){
	var facebook_tags = $('#facebook_tags').val();
	var sex_male 	  = $('#sex_male').val();
	var sex_female 	  = $('#sex_female').val();
	var ft			  = facebook_tags.split(",");
	var obj = [];
	$(ft).each(function(a,b){
		if(!b)return;
		if(b==sex_male || b==sex_female)
		{
			//ft[a] = ft[a+1];
			//ft.length --;
		}else{
			obj.push({userid: b, alias: b});
		}
	});
	obj.push({userid: sex_female, alias: sex_female});

	$('#facebook_tags').select2('data',obj);
});
$('#app_installed_num_not_limit').click(function(){
	var facebook_tags = $('#facebook_tags').val();
	var app_installed_num_less 	  = $('#app_installed_num_less').val();
	var ft			  = facebook_tags.split(",");
	var obj = [];
	$(ft).each(function(a,b){
		if(!b)return;
		if(b==app_installed_num_less)
		{
			//ft[a] = ft[a+1];
			//ft.length --;
		}else{
			obj.push({userid: b, alias: b});
		}
	});
	$('#facebook_tags').select2('data',obj);
});
$('#app_installed_num_normal').click(function(){
	var facebook_tags = $('#facebook_tags').val();
	var app_installed_num_less 	  = $('#app_installed_num_less').val();
	var ft			  = facebook_tags.split(",");
	var obj = [];
	$(ft).each(function(a,b){
		if(!b)return;
		if(b==app_installed_num_less)
		{
			//ft[a] = ft[a+1];
			//ft.length --;
		}else{
			obj.push({userid: b, alias: b});
		}
	});
	$('#facebook_tags').select2('data',obj);
});
$('#app_installed_num_less').click(function(){
	var facebook_tags = $('#facebook_tags').val();
	var app_installed_num_less 	  = $('#app_installed_num_less').val();
	var ft			  = facebook_tags.split(",");
	var obj = [];
	$(ft).each(function(a,b){
		if(!b)return;
		if(b==app_installed_num_less)
		{
			//ft[a] = ft[a+1];
			//ft.length --;
		}else{
			obj.push({userid: b, alias: b});
		}
	});
	obj.push({userid: app_installed_num_less, alias: app_installed_num_less});
	$('#facebook_tags').select2('data',obj);
});