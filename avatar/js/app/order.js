(function(window,document,undefined) {
    var  datatable=null;
    var aoColumns = [
        { "mDataProp": "order_id" ,"bSortable": false},
        { "mDataProp": "nickname" ,"bSortable": false},
        { "mDataProp": "addressee" ,"bSortable": false},
        
        { "mDataProp": "phone" ,"bSortable": false},
        { "mDataProp": "address" ,"bSortable": false},
        { "mDataProp": "remark" ,"bSortable": false},
        { "mDataProp": "express_no" ,"bSortable": false},
        
        { "mDataProp": "total_money" ,"bSortable": false},
        { "mDataProp": "total_amount" ,"bSortable": false},
        { "mDataProp": "detail" ,"bSortable": false},

        { "mDataProp": "create_time" ,"bSortable": false},
        { "mDataProp": "update_time" ,"bSortable": false},

        { "mDataProp": "options" ,"bSortable": false}
    ];


    window.initOrderIndex = initOrderIndex;

    function initOrderIndex(){
        initTable();
    }

    /**
     * [initTable 渲染表格]
     * @return {[type]} [description]
     */
    function initTable()
    {

        datatable = $('#datatable').dataTable(
        {
            "sDom" : "<'dt-top-row'lf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            //"sDom" : "<'dt-top-row'>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "bSortCellsTop" : true,
            "oTableTools" :
            {
                "aButtons" : [
                    "copy",
                    {
                        "sExtends" : "collection",
                        "sButtonText" : 'Save <span class="caret" />',
                        "aButtons" : ["csv", "xls", "pdf"]
                    }
                ],
                "sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
            },
            "fnInitComplete" : function(oSettings, json)
            {
                this.fnAdjustColumnSizing(true);
            },
            "fnDrawCallback": function ( oSettings )
            {
                bindEvent();
                //pageSetUp();
                //runEditable();
                //ideasCount();
            },
            "bFilter":true,
            "sEmptyTable": "Empty Data",
            "bServerSide": true,
            'bPaginate': true, //
            "bProcessing": true, //
            "oLanguage": langage,
//            "bStateSave" : true,
            "sAjaxSource": sAjaxSource,
            // "aaData": data,
            "sAjaxDataProp": "content.data",
            "iDisplayLength": 10,
            "sPaginationType": "bootstrap_full",
            "sScrollX": "100%",
            //"bRetrieve": true,
//            "bScrollCollapse": true,
            "sScrollX": "100%",
            "sScrollXInner": "100%",
            //"bScrollCollapse": false,
            // "aaSorting":[[9,'desc']],
            "aoColumns": aoColumns,

        });
    }


var status='';
var express_no = '';
var express_id = '';
var order_id = '';
var curr_status = '';
    function bindEvent(){
    	$('.updateorder').click(function(){
    		order_id = $(this).data('order-id');
    		curr_status = $(this).data('status');
    		express_no = $(this).data('express_no');
    		if( express_no ){
    			$('#express_no').val( express_no );
    		}
    	});
    

	$('#updateOrderBtn').unbind('click').click(function(){
		express_no = $('#express_no').val();
		status = $('#status').val();
		var $id = $('#status-btn-'+order_id);
		console.log($id)
		$.ajax({
            url: sAjaxSource_order,
            type:'POST',
            data: "order_id=" + order_id  + "&status=" + status + "&express_no=" + express_no+ "&express_id=" + express_id,
            success: function (data) {
                if( data.status == 200  ){
                	var status =  data.content.status;
                    if(status==2){
                        var txt = "待发货";
                        var clas = "btn-info";
                    }else if(status==3){
                        var txt = "配货中";
                        var clas = "btn-danger";
                    }else if(status==4){
                        var txt = "待收货";
                        var clas = "btn-warning";
                    }
                    if( status ){
	                    $id.html(txt);
	                    $id.removeClass(curr_status).addClass(clas).data("status",clas);
                    }
                }else {
                    alert(data.msg);
                    return false;
                }
                $('#myModal').modal('toggle');
            }
        });
        return false;
	});
	
    }
})(window,document,undefined);