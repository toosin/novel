<?php
namespace Com\Weixin;

class WeixinService{
    private $app_id;
    private $app_secret;
    private $request_url;

    /**
     * WeixinService constructor.
     * @param $app_id
     * @param $app_secret
     * @param $request_url
     */
    public function __construct($app_id,$app_secret,$request_url){
        $this->app_id = $app_id;
        $this->app_secret = $app_secret;

        if($request_url[strlen($request_url)-1] == "/"){
            $this->request_url = $request_url;
        }else{
            $this->request_url = $request_url."/";
        }
    }

    /**
     * 授权
     *
     * @param $next_url, 回退地址
     * @param string $scope ,授权模式 ,静默授权选择snsapi_base
     * @param int $isForce , 是否强制
     */
    public function auth($next_url,$scope="snsapi_userinfo",$isForce=0){
        $args = array();
        $args['timestamp'] = time();
        $args['app_id'] = $this->app_id;
        $args['app_secret'] = $this->app_secret;
        $args['next_url'] = $next_url;
        $args['scope'] = $scope;
        if($isForce == 1){
            $args['force_auth'] = 1; //强行授权
        }

        $sign = $this->sign($args);

        unset( $args['app_secret'] );
        $args_str = '';
        foreach ($args as $k=>$v)
        {
            $args_str[]=$k.'='.$v;
        }
        $arg_str = implode('&', $args_str);

        $url = $this->request_url.'user/auth?'.$arg_str.'&sign='.$sign;

        redirect($url);
        die;
    }

    /**
     * 发送红包
     *
     * @param $openid,用户的openid
     * @param $money,金额单位元
     * @param $order，自定义订单号
     * @param $paytype，支付类型，1企业付款，2红包
     * @return string|bool
     *
     */
    public function sendRedPackage($openid,$money,$order,$paytype,$re_user_name=''){

        if($money < 1 ){
            E("红包金额不能小于1元",403);
        }

        $args = array();
        $args['timestamp'] = time();
        $args['app_id'] = $this->app_id;
        $args['app_secret'] = $this->app_secret;
        $args['openid'] = $openid;
        $args['money'] = $money*100;
        $args['ip'] = get_client_ip();
        $args['ua'] = urlencode($_SERVER['HTTP_USER_AGENT']);
        $args['orderid'] = $order;
        $args['paytype'] = $paytype;

        if($paytype == 1){
            $args['check_name'] = 'NO_CHECK';
            $args['re_user_name'] = $re_user_name;
        }

        $sign = $this->sign( $args );

        $args_str = '';
        unset($args['app_secret']);
        #企业支付的时候

        foreach ($args as $k=>$v)
        {
            $args_str[]=$k.'='.$v;
        }
        $arg_str = implode('&', $args_str);

        $url = $this->request_url.'pay/dopay?'.$arg_str.'&sign='.$sign;

        $data = json_decode(https_request($url),true);

        if($data['status'] != 200){
            return $data['msg'];
        }else{
            return true;
        }
    }

    /**
     * @param $args
     * @return string
     */
    private function sign($args)
    {
        $tmpArr = array();
        foreach ($args as $arg){
            $tmpArr[]= $arg;
        }

        sort($tmpArr, SORT_STRING);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);

        return $tmpStr;
    }
}