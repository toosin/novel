<?php
 namespace Com\Fetch;

 class FEncode{
     public function encode($content){

         if(strpos($content,'charset=gbk') || strpos($content,'charset=gb2312')){
             if(strpos($content,'charset=gbk')){
                 $content= str_replace('charset=gbk','charset=utf-8',$content);
                 $content = iconv('gbk','utf-8',$content);
                 $content =  '<meta http-equiv="Content-Type" content="text/html;charset=utf-8">'.$content;
             }elseif (strpos($content,'charset=gb2312')){
                 $content= str_replace('charset=gb2312','charset=utf-8',$content);
                 $content =  '<meta http-equiv="Content-Type" content="text/html;charset=utf-8">'.$content;
             }
         }else{
             $content =  '<meta http-equiv="Content-Type" content="text/html;charset=utf-8">'.$content;
         }

         return $content;
     }
 }