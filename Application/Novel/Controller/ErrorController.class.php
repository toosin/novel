<?php
namespace Demo\Controller;

use Think\Controller;

class ErrorController extends Controller
{

    public function msg()
    {
        $this->display();
    }

    public function focus()
    {
        $this->display();
    }

    public function authfail()
    {
        $this->display();
    }
}