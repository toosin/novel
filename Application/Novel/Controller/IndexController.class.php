<?php
namespace Novel\Controller;

use Com\Fetch\Fetch;
use Common\Model\ConfigModel;
use Think\Controller;

class IndexController extends ControllerBase
{
    public function __construct()
    {
        parent::__construct(true);
        $this->assign("cnzz","1264045561");
    }

    public function index()
    {
        $host = $_SERVER['HTTP_HOST'];
        if($host == C('tzUrl')){
            redirect("http://".C('apiUrl').$_SERVER['REQUEST_URI']);
        }
        $conf = (new ConfigModel())->getConfigs(['top','hot','girl','boy','free']);
        $data = [];
        foreach ($conf as $key=>$val){
            $data[$key] = M('book')->where(['id'=>['in',$val]])->select();
        }
        $read_info = M('user')->where(['id'=>$this->uid])->getField('read_info');
        $this->assign('read_info',$read_info);
        $this->assign('data',$data);
        $this->assign('host',$host);
        $this->display();
    }

    public function getBook(){
        $id = I('id',1457);
        if($id){
            $book = M('book')->where(['id'=>$id])->find();
            $this->assign('book',$book);
            $chapter_asc = M('chapter')->where(['book_id'=>$book['book_id']])->limit(0,5)->field('id,ch_name')->select();
            $chapter_desc = M('chapter')->where(['book_id'=>$book['book_id']])->order('id desc')->field('id,ch_name')->find();
            $this->assign('asc',$chapter_asc);
            $this->assign('desc',$chapter_desc);

            $user = M('user')->where(['id'=>$this->uid])->find();
            $favorite_ids = [];
            if($user['favorite_ids']){
                $favorite_ids = explode(',',$user['favorite_ids']);
            }
            if(in_array($id,$favorite_ids)){
                $this->assign('is_favorite','1');
            }else{
                $addFavorite = I('addFavorite');
                if($addFavorite){
                    $favorite_ids[] = $id;
                    M('user')->where(['id' => $this->uid])
                        ->save(['favorite_ids' => implode(',', $favorite_ids)]);
                    $this->assign('is_favorite','1');
                }
            }
            $this->display('Index/detail');
        }else{
            $this->error();
        }
    }

    public function getChapter(){
        $id = I('id');
        if($id){
            $ch = M('chapter')->where(['id'=>$id])->find();
            if($ch){
                $fromaid = I('fromaid','');
                $prid = I('prid','');
                $book = M('book')->where(['book_id'=>$ch['book_id']])->find();
                $book_id = $book['id'];
                $count = M('chapter')->where(['book_id'=>$ch['book_id'],'id'=>['lt',$ch['id']]])->count();
                if($count>=9){
                    $user = M('user')->where(['id'=>$this->uid])->find();
                    $share_ids = [];
                    if($user['share_ids']){
                        $share_ids = explode(',',$user['share_ids']);
                    }
                    $share_success = I('is_share');
                    if(!in_array($book_id,$share_ids) || !empty($share_success)){
                        redirect("http://".C('shareUrl')."/Index/share?id=$id&book_id=$book_id&is_share=$share_success&prid=$prid&fromaid=$fromaid");
                    }
                }
                $read_info = "<a id='lastread' href='/Index/getChapter?id=$id'>上次阅读:《$book[book_name]》$ch[ch_name]</a>";
                M('user')->where(['id'=>$this->uid])->setField('read_info',$read_info);
                if($ch['is_vip']==2 && $count<14){
                    $this->assign("chapter",$ch);
                    $next_id = M('chapter')->where(['book_id'=>$ch['book_id'],'id'=>['gt',$ch['id']]])->getField('id');
                    $next_id = empty($next_id)?$ch['id']:$next_id;
                    $this->assign("next_id",$next_id);
                    $this->assign("book_id",$book_id);

                    if(!empty($fromaid) && !empty($prid)){
                        $this->assign("fromaid",$fromaid);
                        $this->assign("prid",$prid);
                    }

                    $this->display('Index/capter');
                }else{
                    //https://m.emeixs.com/chapter/2254/2/fromaid/482909879/prid/67213952.html
                    if(!empty($fromaid) && !empty($prid)){
                        redirect('https://m.emeixs.com'.substr($ch['from_source'],0,-8)."$fromaid/prid/$prid.html");
                    }else{
                        redirect('https://m.emeixs.com'.substr($ch['from_source'],0,-8).'482909879/prid/67213952.html');
                    }
                }
            }
        }else{
            $this->error();
        }
    }

    public function share(){
        $id = I('id');
        $fromaid = I('fromaid','');
        $prid = I('prid','');
        $book_id = I('book_id');
        $share_success = I('is_share');
        $md5 = md5("book_share#$book_id");
        if($share_success && $share_success==$md5) {
            $user = M('user')->where(['id'=>$this->uid])->find();
            if($user['share_ids']){
                $share_ids = explode(',',$user['share_ids']);
            }
            $share_ids[] = $book_id;
            M('user')->where(['id' => $this->uid])->save(['share_ids' => implode(',', $share_ids)]);
            /*$this->assign("go_url","/Index/getChapter?id=$id");
            $this->assign("pic","/novel/images/hb.jpg");
            $this->display('Index/share');*/
            redirect("http://".C('apiUrl')."/Index/getChapter?id=$id&prid=$prid&fromaid=$fromaid");
            return;
        }else{
            $this->assign("go_url","/Index/getChapter?id=$id&is_share=$md5&prid=$prid&fromaid=$fromaid");
            $this->assign("is_share","1");
            $this->assign("pic","/novel/images/hb.jpg");
            $this->display('Index/share');
            return;
        }
    }

    public function getChapterList(){
        $id = I('id');
        $page = I('page',1);
        $order = I('order','asc');
        if($id){
            $ch = M('chapter')->where(['book_id'=>$id])->order("id $order")->field('id,ch_name,is_vip')->limit(0,$page*50)->select();
            $this->assign("list",$ch);
            $this->assign("id",$id);
            $this->assign("next",++$page);
            $this->assign("order",$order);
            $this->display('Index/showlist');
        }else{
            $this->error();
        }
    }

    public function getCategory(){
        $type = I('type');
        $page = I('page',0);
        $where = [];
        if($type){
            $where['book_type'] = $type;
        }
        $books = M('book')->where($where)->limit($page*10,10)->select();
        $count = M('book')->where($where)->count();
        $this->assign("books",$books);
        $this->assign("total_page",ceil($count/10));
        $this->assign("next",$page==(ceil($count/10))?$page:++$page);
        $this->assign("nextpage",$page>=ceil($count/10)?($page-1):$page);
        $this->assign("type",$type);
        $this->display('Index/category');
    }

    public function getBookCase(){
        $user = M('user')->where(['id'=>$this->uid])->find();
        if($user['share_ids'] || $user['favorite_ids']){
            $ids = (empty($user['share_ids'])?"":$user['share_ids']).(empty((empty($user['share_ids'])?"":$user['share_ids']))?$user['favorite_ids']:(','.$user['favorite_ids']));
            $books = M('book')->where(['id'=>['in',$ids]])->select();
            $this->assign("books",$books);
        }
        $this->assign('read_info',$user['read_info']);
        $this->display('Index/bookcase');
    }

}