<?php
//中文语言包
return array(
	'cms_name'=>'阿凡达',
	'cms_submit'=>'提交',
	'cms_success'=>'提交成功',
	'cms_other_error'=>'系统异常',
	'cms_other_nodata'=>'暂时没有数据',
	'cms_cancel'=>'取消',
	
	'cms_edit_success'=>'操作成功',
	'cms_edit_fail'=>'操作失败',

	'dashboard'=>'索引页',
	//表格语言包
	"sZeroRecords"=>"没有检索到数据",
	"sInfo"=> "当前数据为从第 _START_ 到第 _END_ 条数据；总共有 _TOTAL_ 条记录",
	"sInfoEmtpy"=> "没有数据",
	"sProcessing"=> "正在加载数据...",
	'sFirst'=>'首页',
	'sPrevious'=>'前一页',
	'sNext'=>'后一页',
	'sLast'=>'尾页',

	'no_priv'=>'您没有操作此功能的权限',


	'export'=>'导出',
	'save'=>'保存',
	'copy'=>'复制',
	'edit'=>'编辑',
	'add'=>'添加',
	'delete'=>'删除',
	'open'=>'打开',
	'close'=>'关闭',

	'no_priv_page'=>'您没有权限访问该页面!',
);