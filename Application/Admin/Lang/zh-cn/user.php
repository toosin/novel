<?php
//中文语言包
return array(
		'user_add'=>'用户添加',
		'user_list'=>'用户管理',
		'user_name'=>'姓名',
		'user_email'=>'邮箱',
		'user_phone'=>'电话',
		'user_pwd'=>'密码',
		'user_leader'=>'领导',
		'user_leader_email'=>'领导邮箱',
		'user_department'=>'部门',
		'user_lang'=>'系统默认语言',
		'user_pic'=>'头像',
		'user_last_ip'=>'最后登录IP',
		'user_last_time'=>'最后登录时间',
		'user_status'=>'状态',
		'user_empty_prompt'=>'字段不可为空',
		'user_phone_error'=>'手机号格式有误',
		'user_email_error'=>'邮箱格式有误',
		'user_pwd_error'=>'密码请控制在6到20个字符',
		'user_email_exist'=>'该邮箱已经存在了',
		'user_status_0'=>'锁定',
		'user_status_1'=>'开启',
		'user_edit'=>'编辑',
		
		'user_pwd_edit_tip'=>'不修改请留空',
		'user_edit_faild'=>'修改内容为空',
		
		'user_email_tip'=>'用户公司邮箱',
		'user_pwd_tip'=>'用户初始密码',
		'user_name_tip'=>'用户真实姓名',
		'user_phone_tip'=>'用户手机号',
		'user_leader_tip'=>'用户领导姓名',
		'user_leaderemail_tip'=>'用户领导邮箱，方便联系',
		'user_department_tip'=>'用户所属部门',
		'user_pwd_tip2'=>'用户密码',

		'user_role'=>'角色',
		'user_role_list'=>'角色管理',
		
		'user_add_pwd_tip'=>'初始密码系统将自动生成发送到用户邮箱，请君切莫担忧；',
		
);