<?php
//中文语言包
return array(
		'login_title'=>'登 录 ',
		'login_email'=>'您的邮箱',
		'login_email_tip'=>'在此输入邮箱',
		'login_pwd'=>'密码',
		'login_pwd_tip'=>'在此输入密码',
		'login_email_error'=>'邮箱格式错误',
		'login_pwd_error'=>'密码错误',
		'login_empty_prompt'=>'请填写完整您的信息',
		'login_nouser_info'=>'您的帐号不存在',
		'login_status_info'=>'您的帐号被锁定请联系管理员解锁',
		'login_success'=>'登陆成功',
		'no_role_priv'=>'你的帐号尚未角色授权',
		'no_role_priv_index'=>'你的帐号未授权首页',
);