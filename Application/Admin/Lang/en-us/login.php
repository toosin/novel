<?php
//中文语言包
return array(
		'login_title'=>'Sign In ',
		'login_email'=>'E-mail',
		'login_email_tip'=>'Please enter email address',
		'login_pwd'=>'Password',
		'login_pwd_tip'=>'Enter your password',
		'login_email_error'=>'Email format is wrong',
		'login_pwd_error'=>'Password mistake',
		'login_empty_prompt'=>'Please complete your information',
		'login_nouser_info'=>'account does not exist',
		'login_status_info'=>'account is locked, please contact your administrator to unlock',
		'login_success'=>'Login successful',
		'no_role_priv'=>'Your account has not been authorized role',
		'no_role_priv_index'=>'Unauthorized home page of your account',
);