<?php
//英文语言包
return array(
	'cms_name'=>'Avatar',
	'cms_submit'=>'Submit',
	'cms_success'=>'Submitted successfully',
	'cms_other_error'=>'System error',
	'cms_other_nodata'=>'Temporarily no data',
	'cms_cancel'=>'Cancel',
	
	'cms_edit_success'=>'Successful',
	'cms_edit_fail'=>'Failed',

	'dashboard'=>'Dashboard',
	//表格语言包
	'sZeroRecords'=>'no data~',
	'sInfo'=>'Showing  _START_  to _END_ of _TOTAL_ entries',
	'sInfoEmtpy'=>'Temporarily no data',
	'sProcessing'=>'loading data ...',
	'sFirst'=>'First',
	'sPrevious'=>'Previous',
	'sNext'=>'Next',
	'sLast'=>'Last',

	'no_priv'=>'You do not have permission to operating this function',

	'export'=>'Export',
	'save'=>'Save',
	'copy'=>'Copy',
	'edit'=>'Edit',
	'add'=>'Add',
	'delete'=>'Delete',
	'open'=>'Open',
	'close'=>'Close',

	'no_priv_page'=>'You do not have permission to access this page!',

);