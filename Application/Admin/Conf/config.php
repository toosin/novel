<?php
//配置
return array(
    'URL_MODEL' =>0,
    'SESSION_PREFIX' =>  'admin',
    'SUB_PATH'=>"/",
    
    'UPLOAD_PATH' => '/static/upload/thumb/',
    
    'DEFAULT_THEME'    =>    '',
    
    'LANG_SWITCH_ON' => true,           // 开启语言包功能
    'LANG_AUTO_DETECT' => true,         // 自动侦测语言 开启多语言功能后有效
    'LANG_LIST'        => 'zh-cn,en-us',// 允许切换的语言列表 用逗号分隔
    'VAR_LANGUAGE'     => 'l',          // 默认语言切换变量
);
?>