<?php

namespace Admin\Model;
use Think\Model;
class RolepermissionsModel  extends Model{
    protected $tableName = 'role_permissions';
	public function AddRole($data){
		return $this->data($data)->add();
	}
	public function AddRoleall($data){
		return $this->addAll($data);
	}
	public function FindRole($where,$field='*'){
		return $this->where($where)->field($field)->find();
	}
	public function UpdataRole($where,$data){
		return $this->data($data)->where($where)->save();
	}
	public function RoleDel($where){
		return $this->where($where)->delete();
	}
	public function SelectRole($where=1,$field='*'){
		return $this->where($where)->field($field)->select();
	}
}
