<?php

namespace Admin\Model;
use Think\Model;
class AdminModel  extends Model{
	protected $tableName = 'admin';
	public function AddUser($data){
		return $this->data($data)->add();
	}
	public function FindUser($where){
		return $this->where($where)->find();
	}
	public function UpdataInfo($where,$data){
		return $this->data($data)->where($where)->save();
	}
	public function SelectUser($where=1){
		return $this->where($where)->select();
	}
}
