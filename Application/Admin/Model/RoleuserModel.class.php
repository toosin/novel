<?php

namespace Admin\Model;
use Think\Model;
class RoleuserModel  extends Model{
	protected $tableName = 'role_user';
	public function AddRole($data){
		return $this->data($data)->add();
	}
	public function AddRoleall($data){
		return $this->addAll($data);
	}
	public function FindRole($where){
		return $this->where($where)->find();
	}
	public function UpdataRole($where,$data){
		return $this->data($data)->where($where)->save();
	}
	public function RoleuserDel($where){
		return $this->where($where)->delete();
	}
	public function SelectRole($where=1,$field='*'){
		return $this->where($where)->field($field)->select();
	}
	public function RoleLeftselct($where){
		$pre = C('DB_PREFIX');
		return $this->join('LEFT JOIN '.$pre.'role ON '.$pre.'role_user.role_id = '.$pre.'role.role_id')->where($where)->field('name,en_name')->select();
	}
}
