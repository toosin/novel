<?php
namespace Admin\Model;
use Think\Model;

class UserTaskModel extends Model {
    protected $tableName = 'user_task';

    /**
     * [getTotayMoney 获取用户今日收益]
     * @param  [type] $uid [description]
     * @return [type]      [description]
     */
    public function getTotayMoney( $uid ){
        $usertask = $this;

        $fans = D('Fans');
        $fans_list = $fans->getFans( $uid );
        $fans_ids = array();
        foreach ($fans_list as $key => $value) {
            $fans_ids[] = $value['fansid'];
        }

        //今日收益
        $today_money = 0;
        $where = array();
        $where['userid'] = $uid;
        $where['date'] = date("Ymd");
        // $where['taskid'] = 1;
        $m1 = $usertask->where( $where )->select();

        if( $m1 ){
            foreach ($m1 as $key => $value) {
                $today_money += $value['taskmoney'];
            }
        }

        if( $fans_ids ){
            //粉丝今日的收入
            $where = array();
            $where['userid'] = array('in',$fans_ids);
            $where['date'] = date("Ymd");
            $where['recursive'] = 1;
            $m2 = $usertask->where( $where )->select();
            if( $m2 ){
                $tmp_money = 0;
                foreach ($m2 as $key => $value) {
                    $tmp_money += $value['taskmoney'];
                }
                $tmp_money = number_format($tmp_money*0.1,2);
                $today_money +=$tmp_money;
            }
        }

        return $today_money;
    }

    /**
     * [getYesterdayMoney 获取昨日收益]
     * @param  [type] $uid [description]
     * @return [type]      [description]
     */
    public function getYesterdayMoney( $uid ){
        $usertask = $this;

        $fans = D('Fans');
        $fans_list = $fans->getFans( $uid );
        $fans_ids = array();
        foreach ($fans_list as $key => $value) {
            $fans_ids[] = $value['fansid'];
        }
        //昨日收益
        $lastday_money = 0;
        $where = array();
        $where['userid'] = $uid;
        $where['date'] = date("Ymd",strtotime('-1 day'));
        // $where['taskid'] = 1;
        $m1 = $usertask->where( $where )->select();
        if( $m1 ){
            if( $m1 ){
                foreach ($m1 as $key => $value) {
                    $lastday_money += $value['taskmoney'];
                }
            }
        }

        if($fans_ids){
            //粉丝昨日的收入
            $where = array();
            $where['userid'] = array('in',$fans_ids);
            $where['date'] = date("Ymd",strtotime('-1 day'));
            $where['recursive'] = 1;
            $m2 = $usertask->where( $where )->select();
            if( $m2 ){
                $tmp_money = 0;
                foreach ($m2 as $key => $value) {
                    $tmp_money += $value['taskmoney'];
                }
                $tmp_money = number_format($tmp_money*0.1,2);
                $lastday_money +=$tmp_money;
            }
        }

        return $lastday_money;
    }
}