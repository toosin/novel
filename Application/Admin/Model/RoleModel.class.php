<?php

namespace Admin\Model;
use Think\Model;
class RoleModel  extends Model{
	protected $tableName = 'role';
	public function AddRole($data){
		return $this->data($data)->add();
	}
	public function FindRole($where){
		return $this->where($where)->find();
	}
	public function UpdataRole($where,$data){
		return $this->data($data)->where($where)->save();
	}
	public function RoleDel($where){
		return $this->where($where)->delete();
	}
	public function SelectRole(){
		return $this->select();
	}
}
