<?php
namespace Admin\Model;
use Think\Model;

class FansModel extends Model {
	protected $tableName = 'fans';
	
	public function getFans( $userid ) {
		$where = array();
		$where['userid'] = $userid;
		$fans_list = $this->where( $where )->select();
		
		return $fans_list;
	}
}