<?php

namespace Admin\Controller;


/**
 * 程序框架基本页面，包括首页、菜单、锁屏等页面
 */
use Think\Controller;
use Common\Model\OpenIdModel;

class DataController extends BaseController 
{
	/**
	 * dashboard
	 * @nav L('dashboard')
	 * 
	 * @return [type] [description]
	 */
	public function dashboard() 
	{
		$controller = I ( 'get._c' );
		$action = I ( 'get._a' );
		// 数据接口，转发到相应的action
		if ($action) {
			$allow_controller = array (
					'DataService' 
			);
			$controller = $controller ? $controller : CONTROLLER_NAME;
			R ( $controller . "/" . $action );
			return true;
		}
		$db_prefix = C ( 'DB_PREFIX' );
		
		
		
		$u = D ( 'User' );
		$openid_obj = new OpenIdModel();
		$total_user = $u->count ();
		$unsubscribe_user = $openid_obj->where ( "state=0 and wxhao='".C('WX_HAO')."'" )->count ();
		$this->assign ( "total_user", $total_user );
		$this->assign ( "unsubscribe_user", $unsubscribe_user );
		
		$today_newuser = S ( 'today_newuser' );
		if (! $today_newuser) {
			$where = array ();
			$today = date ( "Y-m-d 00:00:00" );
			$where ['addtime'] = array (
					array (
							'egt',
							$today 
					) 
			);
			;
			$today_newuser = $u->where ( $where )->count ();
			S ( 'today_newuser', $today_newuser, 600 );
		}
		$this->assign ( "today_newuser", $today_newuser );
		
		$yesterday_newuser = S ( 'yesterday_newuser' );
		if (! $yesterday_newuser) {
			$where = array ();
			$yesterday = date ( "Y-m-d 00:00:00", strtotime ( '-1 day' ) );
			$where ['addtime'] = array (
					array (
							'egt',
							$yesterday 
					),
					array (
							'lt',
							$today 
					) 
			);
			;
			$yesterday_newuser = $u->where ( $where )->count ();
			S ( 'yesterday_newuser', $yesterday_newuser, 600 );
		}
		$this->assign ( "yesterday_newuser", $yesterday_newuser );
		
		
		$t1 =  ( date ( "Y-m-d 00:00:00" ) );
		$today_active_user = S ( 'today_active_user' );
		if (! $today_active_user) {
			$u = D ( 'User' );
			$where = array ();
			$where ['updatetime'] = array (
					'egt',
					$t1 
			);
			$today_active_user = $u->where ( $where )->count ();
			S ( 'today_active_user', $today_active_user, 1800 );
		}
		$this->assign ( 'today_active_user', $today_active_user );
		
		$t2 =  ( date ( "Y-m-d 00:00:00", strtotime ( '-1 day' ) ) );
		$yesterday_active_user = S ( 'yesterday_active_user' );
		if (! $yesterday_active_user) {
			$where = array ();
			$where ['updatetime'] = array (
					array (
							'egt',
							$t2 
					),
					array (
							'lt',
							$t1 
					) 
			);
			$yesterday_active_user = $u->where ( $where )->count ();
			S ( 'yesterday_active_user', $yesterday_active_user, 1800 );
		}
		$this->assign ( 'yesterday_active_user', $yesterday_active_user );
		
		$this->display ();
	}
}