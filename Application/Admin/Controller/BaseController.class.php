<?php
namespace Admin\Controller;
use Think\Controller;

class BaseController extends Controller {
	public function __construct() {
		parent::__construct();
		//登陆检测
		tag("check_login");
	}

	public function _empty() {
		if( IS_AJAX ){
            $data['status']  = 404;
            $data['content'] = array();
            $data['msg'] = 'Action Not Found';
            $this->ajaxReturn($data);
        }else{
            echo "empty Action";
        }
	}

    protected function log( $content ){
        $logs = D('Log');
        $data = array();
        $data['text'] = $content;
        $logs->add($data);
    }
}
