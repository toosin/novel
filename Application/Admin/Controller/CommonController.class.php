<?php
// +----------------------------------------------------------------------
// | Just do it
// +----------------------------------------------------------------------
// | Author: shioujun <shioujun@gmail.com>
// +----------------------------------------------------------------------
// | Since: 2014-08-05 15:16
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Admin\Model\ToolModel;
use Think\Controller;
use Think\Crypt\Driver\Think;

class CommonController extends BaseController
{
	/**
	 * 上传
	 * @privilege public
	 * @return [type] [description]
	 */
    public function upload() {
        $ret['status']  = 200;
        $ret['content'] = '';
        $ret['msg'] = '';

        try{


        	            	
        	$upload = new \Think\Upload();// 实例化上传类
        	$upload->maxSize   =     3145728 ;// 设置附件上传大小
        	$upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        	$upload->subName = array('date',"Ymd");
        	
        	if( defined("SAE_ENV") ){
        		$upload->rootPath = "./gimg/";
        		//$upload->savePath  =      './gime/upload/'; // 设置附件上传目录
        	}else{
        		$dir = APP_ROOT.'/avatar'. C('UPLOAD_PATH').'';
        		$date = date('Ymd');
        		
        		$subdir = "{$date}/";
        		if(!is_dir($dir.$subdir)){
        			$m = mkdirs($dir.$subdir,0775);
        			if( !$m ) {
        				E('创建目录失败：'.$dir.$subdir);
        			}
        		}
        		if( !file_exists($dir.$subdir) ){
        			E('上传路径不存在！');
        		}        
				
        		
        		$upload->rootPath  =  $dir; // 设置附件上传目录
        	}
        	$info = $upload->upload();
        	if( !$info ){
        		E( $upload->getError() );
        	}
        	if( defined("SAE_ENV") ){
        		$url = $info['Filedata']['url'];
        	}else{
        		$url =  C('UPLOAD_PATH').$info['Filedata']['savepath'].$info['Filedata']['savename'];
        	}
        	$ret['content'] = $url;
        	
        }catch(\Exception $e){
            $ret['msg'] = $e->getMessage();
            $ret['status'] = $e->getCode();
        }
        $this->ajaxReturn( $ret );
    }
}