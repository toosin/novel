<?php
// +----------------------------------------------------------------------
// | 一个可在后台设置自动消息回复与 编辑微信菜单的功用
// +----------------------------------------------------------------------
// | Since: 2016-05-26 14:54
// +----------------------------------------------------------------------
// | Author: dengsixian <1741159138@qq.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

use Think\Controller;


class WechatController extends Controller
{
    public function __construct()
    {
        parent::__construct();   
    }

    /**
     * 刷新token
     * @return [type] [description]
     */
    public function refresh_token()
    {
        get_token(true);
        $this->success('get_token刷新成功', '', 1);
    }

    /**
     * 文本消息回复编辑
     * @return [type] [description]
     */
    public function wx_basic_reply()
    {
        $id = I('get.id');
        $data = [];
        if($id) {
            $basic_obj   = M('wx_basic_reply'); // 实例化basic_obj对象
            $where = ['id' => $id];
            $data      = $basic_obj->where($where)->find();
        }
        $this->assign(compact('data'));

        $this->display();
    }

    /**
     * 文本消息回复列表
     * @return [type] [description]
     */
    public function wx_basic_reply_list()
    {
        $basic_obj   = M('wx_basic_reply'); // 实例化basic_obj对象
        $where = [];
        $count      = $basic_obj->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page3($count,10);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $basic_obj
                ->where($where)
                ->order('last_time desc')
                ->limit($Page->firstRow.','.$Page->listRows)
                ->select();
        $this->assign('list',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出

    	$this->display();
    }

    /**
     * 删除自动回复消息
     * @return [type] [description]
     */
    public function wx_basic_reply_del()
    {
        if( I('get.id') )
        {   $config_obj = D('wx_basic_reply');
            $conditon['id'] = I('get.id');
            if(in_array(I('get.id'),[1, 2])){
                // E('此条回复不能被删除');
                $this->error('此条内容不能被删除', '', 1);
            }
            $res = $config_obj->where($conditon)->delete();
            $this->success('删除成功', '', 1);
        }else {
            E('id非法操作');
        }
    }

    /**
     * 创建菜单
     * @return [type] [description]
     */
    public function create_menu()
    {
        if(I('post.group'))
        {
            $button_arr = I('post.group');
            $button = $button_arr;
            $data = JSON($button);
            $url = 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token=' . get_token();
            $rs = https_request($url, $data );
            echo $rs;
            exit;
        }else {
            E('id非法操作');
        }
    }

    /**
     * 微信菜单编辑
     * @return [type] [description]
     */
    public function wx_menu()
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/menu/get?access_token=' . get_token();
        $data = file_get_contents($url);
        $data = json_decode($data, true);
        $history_menu = 'null';
        if(isset($data['menu'])) {
            $history_menu = json_encode($data['menu']);
        }
        $this->assign(compact('history_menu', 'data'));
    	$this->display();
    }

    /**
     * 保存自动回复消息
     * @return [type] [description]
     */
    public function save_wx_basic_reply()
    {
    	$config_obj = D('wx_basic_reply');
    	$conditon['id'] = I('post.id');
        $data['content'] = I('post.content');
        $data['name'] = I('post.name');
        $data['keyword'] = $keyword = I('post.keyword');
        if( I('post.id') )
        {
            $res = $config_obj->where($conditon)->data($data)->save();
            $this->success('更新成功', I('post.backUrl', ''), 1);
        }else {
            if( stristr($keyword, ',') ) {   
                $k_arr = explode(',', $keyword);
                foreach ((array)$k_arr as $key => $value) {
                    $newdata['content'] = $data['content'];
                    $newdata['name'] = $data['name'];
                    $newdata['keyword'] = $value;
                    $res = $config_obj->data($newdata)->add();
                }
            }else {
                $res = $config_obj->data($data)->add();
            }
            $this->success('添加成功', I('post.backUrl', ''), 1);
        }
    }

    /**
     * 日志功能
     * @return [type] [description]
     */
    public function wx_error_log()
    {
        $log_obj   = M('log'); // 实例化log_obj对象
        $where = [];
        $count      = $log_obj->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page3($count,10);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $log_obj
                ->where($where)
                ->order('create_time desc')
                ->limit($Page->firstRow.','.$Page->listRows)
                ->select();
        $this->assign('list',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $this->display();
    }

}
