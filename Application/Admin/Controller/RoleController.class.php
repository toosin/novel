<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Exception;
class RoleController extends BaseController {
	private $model ,$role;
	function __construct() {
		parent::__construct();
		$this->model = D('Menu');
		$this->role_model = D('Role');
	}
	/**
	 * 角色列表
	 * @nav L('role_list')
	 */
    public function index() {
    	if(I('get.data','')){
    		$rel = $this->role_model->SelectRole();
    		$this->ajaxReturn( ajax_return_join(200, $rel, 'cms_success'));
    	}else{
    		$this->display();
    	}
    	
    }

    /**
     * 添加角色
     *@nav  L('role_add')
    */     
    public function roleAdd() {
    
    	$post = I('post.');
        if($post['dosubmit']){
			if(!trim($post['name']) || !trim($post['en_name'])){
				$this->ajaxReturn( ajax_return_join(202, '', 'role_no_name'));exit;
			}
        	unset($post['dosubmit']);
        	$id = $this->role_model->AddRole($post);
        	if($id){
        		$this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;
        	}
        }else{
        	$this->display();
        }
    }
    /**
     * 修改角色信息
     * @nav L('role_edit')
     */
	public function roleEdit(){
		if(I('get.id')){
			$info = $this->role_model->FindRole("role_id='".I('get.id')."'");
			if(I('post.dosubmit')){
				$post = I('post.');
				if(!trim($post['name']) || !trim($post['en_name'])){
					$this->ajaxReturn( ajax_return_join(202, '', 'role_no_name'));exit;
				}
				unset($post['dosubmit']);
				$where = 'role_id='.$info['role_id'];
				$rel = $this->role_model->UpdataRole($where,$post);
				if($rel){
					$this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;
				}else{
					$this->ajaxReturn( ajax_return_join(302, '', 'role_edit_faild'));exit;
				}
			}else{
				$this->assign('rel',$info);
				$this->display();
			}
		}else{
			redirect(U('user/index'));
		}
	}
	/**
	 * 删除角色
	 */
	public function roleDelete(){
		if(I('post.id')){
			$role_user_module = D('Roleuser');
			$where = array('role_id'=>I('post.id'));
			$this->role_model->RoleDel($where);
			$role_user_module->RoleuserDel($where);
			$Rolepermissions_model = D('Rolepermissions');
			$drel = $Rolepermissions_model->RoleDel($where);
			$this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;
		}else{
			redirect(U('user/index'));
		}
	}
	/**
	 * 设置角色权限
	 * @nav L('role_permissions')
	 */
	public function rolePriv(){
		if(I('get.id')){
			$Rolepermissions_model = D('Rolepermissions');
			$info = $this->role_model->FindRole("role_id='".I('get.id')."'");
			if(I('post.dosubmit')){
				$role = I('post.role');
				if(!$role){
					$this->ajaxReturn( ajax_return_join(202, '', 'no_role_tip'));exit;
				}
				#清理现有权限
				$where = array('role_id'=>I('get.id'));
				$drel = $Rolepermissions_model->RoleDel($where);
				#插入新权限
				foreach($role as $k=>$v){
					$data[] = array('role_id'=>I('get.id'),'per_menu_id'=>$v);
				}
				$rel = $Rolepermissions_model->AddRoleall($data);
				if($rel){
					$this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;
				}
				
				
			}else{
				$rel = $this->model->getMenu();
				$reltree = $this->model->getTree($rel);
				#查询已有权限
				$where = array('role_id'=>I('get.id'));
				$Rolepermissions = $Rolepermissions_model->SelectRole($where,'per_menu_id');
				foreach($Rolepermissions as $key=>$val){
					$this->role[] = $val['per_menu_id'];
				}
				foreach ($reltree as $key => $value) {
					$str .= $this->generateHTML($value);
				}
				$this->assign('html',$str);
				$this->assign('rel',$info);
				$this->display();
			}
		}else{
			redirect(U('user/index'));
		}
	}
	/**
	 * 树形结构生成html
	 * @param array $value
	 * @return string
	 */
	private function generateHTML( $value ){
	
		$menu = $value->menu;
	
		$title = $menu['name'];
		//解析语言包
		if( preg_match('/L\(.+?\)/', $title) ) {
			eval('$title = '.$title.";" );
		}else{
			//判断语言
			if( I('cookie.think_language')=='en-us' ) {
				$title = $menu['en_name'];
			}
		}
	
		
	
		$close_span = '';
		if( $menu['icon'] ){
			$icon= '<i class="fa fa-lg fa-fw '.$menu['icon'].'"></i>&nbsp;';
		}
		if(in_array($menu['id'], $this->role)){
			$checked = 'checked=checked';		
		}
		if($value->nodes){
			$isplus = '<i class="fa fa-lg fa-plus-circle"></i>';
		}else{
			$isplus = '<i class="fa fa-lg" >&nbsp;&nbsp;&nbsp;&nbsp;</i>';
			
		}
		
		if( $menu['pid'] ==0 )
		{
			$str =  '<li>'.$isplus;
			$close_span = '</span>';
			$close_label = '</label>';
			$str .= '<span class="menu-item-parent" style="padding:5px;line-height: 24px">'.$icon.'<label class="checkbox"  style="float:left"><input type="checkbox" name="role[]" value="'.$menu['id'].'" '.$checked.' onchange="check_box_role(this.value)" id="role_box_'.$menu['id'].'"><i></i>';
		}elseif(!$value->nodes){
			$str =  '<li style="display:none;">';
			$close_span = '</label> </span>';
			$str .= '<span> '.$icon.'<label class="checkbox">
					<input type="checkbox" name="role[]" value="'.$menu['id'].'" '.$checked.'>
					<i></i>';
		}else{
			$str =  '<li style="display:none;">'.$isplus;
			$close_span = '</span>';
			$close_label = '</label>';
			$str .= '<span style="padding:5px;line-height: 24px">'.$icon.'<label class="checkbox" style="float:left"><input type="checkbox" '.$checked.' name="role[]" value="'.$menu['id'].'" onchange="check_box_role(this.value)" id="role_box_'.$menu['id'].'">
					<i></i>';
		}
		
		
		
		$str .= $close_label.$title.$close_span;
	
		if( $value->nodes ){
			$str .= "<ul class='checked_".$menu['id']."'>";
			foreach($value->nodes as $v  ){
				$str .= $this->generateHTML($v);
			}
			$str .= "</ul>";
		}
		$str .= "</li>";
	
		return $str;
	}
    public function _empty() {
    	echo "empty action";
    }
   
    
}
