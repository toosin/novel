<?php
namespace Admin\Controller;
use Think\Controller;

class EmptyController extends Controller {
	public function index( ) {
        if( IS_AJAX ){
            $data['status']  = 404;
            $data['content'] = array();
            $data['msg'] = 'Controller Not Found';
            $this->ajaxReturn($data);
        }else{
            echo "empty controller";
        }
	}

	public function _empty( ) {
		if( IS_AJAX ){
            $data['status']  = 404;
            $data['content'] = array();
            $data['msg'] = 'Action Not Found';
            $this->ajaxReturn($data);
        }else{
            echo "empty Action";
        }
    }
}