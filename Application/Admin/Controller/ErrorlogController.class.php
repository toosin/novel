<?php
// +----------------------------------------------------------------------
// | 日志记录显示 控制器
// +----------------------------------------------------------------------
// | Author: dengsixian <1741159138@qq.com>
// +----------------------------------------------------------------------
// | Since: 2016-07-05 16:13:00
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Think\Controller;
use Think\Model;


class ErrorlogController extends BaseController
{
    /**
     * 错误日志记录
     * @return [type] [description]
     */
    public function index()
    {
        $controller = I( 'get._c' );
        $action     = I( 'get._a' );
        //数据接口，转发到相应的action
        if( $action )
        {
            $controller = $controller ? $controller : CONTROLLER_NAME;
            R($controller."/".$action);
            return true;
        }

        $data = array(
                ['name' => 'ID'],
                ['name' => '错误记录'],
                ['name' => '添加时间'],
            );
        $show_data = array_values($data);

        $json_data = '[
                        { "mDataProp": "id" ,"bSortable": true},
                        { "mDataProp": "text" ,"bSortable": false},
                        { "mDataProp": "create_time" ,"bSortable": false},
                    ]';
        $this->assign(compact('show_data', 'json_data'));
        $this->display('Autohtml/index');
    }

    /**
     * [userdata description]
     * @return [type] [description]
     */
    public function dataList(){
        $ret = array();
        $ret['status'] = 200;
        $ret['msg'] = "ok";
        $ret['content'] = array();

        try{
            $offset = I("iDisplayStart",0,"intval");
            $size = I("iDisplayLength",10,"intval");
//             $offset = ($page - 1) * $size;

            $where = array();
            $keyword = I('sSearch');
            if( $keyword ) {
                $where['text']  = array('like','%'.$keyword.'%');
                // $where['content']  = array('like','%'.$keyword.'%');
                $where['_logic'] = 'or';
            }
            $error_obj = D("log");
            $data = $error_obj->where( $where )->limit("{$offset},{$size}")->order("id desc")->select();
            header("sql : ".$error_obj->getLastSql() );
            
            $ids = array();
            foreach ($data as $key=>$val){
            	$ids[ $val['id'] ] = $val['id'];
            }
            $is_focus_data = array();
            $ids = array_values($ids);
            foreach ($data as $key => $value) {
            }
            $total = $error_obj->where( $where )->count();
            $rs1 = array();
            $rs1['data'] = $data?$data:array();
            $ret['iTotalDisplayRecords'] = $total?$total:0;
            $ret['iTotalRecords'] = $total?$total:0;

            $ret['content'] = $rs1;

        }catch(\Exception $e){
            $ret['status'] = $e->getCode();
            $ret['msg'] = $e->getMessage();
        }
        $this->ajaxReturn($ret);
    }
}
