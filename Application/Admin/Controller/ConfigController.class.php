<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Exception;
class ConfigController extends BaseController {

	
	public function newindex(){
	    $config = D("Config");
	    $where = array();
	    $where['manual_config'] = 1;
	    $datas = $config->where($where)->select();
	    
	    $this->assign("data",$datas);
	    $this->display();
	}
	
	public function save(){
		$ret = array();
		$ret['status'] = 200;
		$ret['msg'] = "ok";
		$ret['data'] = array();
		 
		try{
			$config = D("Config");
			
			$config_data = I("post." ,  '' , ''  );
			foreach ($config_data as $name=> $item ){
			    $rs = $config->sets($name,$item);
			    if( $rs===false ){
			        E("保存数据错误。key:".$name.' 。value: '.$item);
			    }
			}
			
			
		}catch(\Exception $e){
			$ret['status'] = $e->getCode();
			$ret['msg'] = $e->getMessage();
		}
		$this->ajaxReturn($ret);
	}
	
	public function fenhong()
	{
		if(IS_POST){
		    $ret = array();
		    $ret['status'] = 200;
		    $ret['msg'] = "ok";
		    $ret['data'] = array();
		    	
		    try{
		        $task_obj = D('task');
		        
		        $data = array();
		        $data['fenhong1'] = I('fenhong1',0,'floatval');
		        $data['fenhong2'] = I('fenhong2',0,'floatval');
		        $data['fenhong3'] = I('fenhong3',0,'floatval');
		        $data['fenhong4'] = I('fenhong4',0,'floatval');
		        $data['fenhong5'] = I('fenhong5',0,'floatval');
		        foreach ($data as $item){
		            if( empty($item) ){
		        	     E('参数错误',400);
		            }
		        }
		        
		        foreach ($data as $key=>$item){
		            $update = array();
		            $update['task_money'] = $item;
		            $update['update_time'] = date('Y-m-d H:i:s');
		            $where = array();
		            $where['key'] = $key; 
		            $task_obj->where($where)->save($update);
		        }
		    }catch(\Exception $e){
		        $ret['status'] = $e->getCode();
		        $ret['msg'] = $e->getMessage();
		    }
		    $this->ajaxReturn($ret);
		}
		
		$task_obj = D('task');
		$data = $task_obj->select();
		$new_data = array();
		foreach ($data as $item)
		{
			$new_data[$item['key']] = $item['task_money'];
		}
		$this->assign('data',$new_data);
		$this->display();
	}
}