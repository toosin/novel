<?php

use Org\Util\String;
/**
 * 项目公共函数库
 */

/**
 * pwd key生成
 */
function pwd_key(){
	$str = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$len = strlen($str)-1;
	$randString = '';
	for($i = 0;$i < 5;$i ++){
		$num = mt_rand(0, $len);
		$randString .= $str[$num];
	}
	return $randString ;
}

/**
 * [myTrace 调试函数]
 * @param  [type]  $var     [要打印输出的变量]
 * @param  boolean $is_exit [是否需要exit程序]
 * @return [type]           [description]
 */
function myTrace( $var , $is_exit=true ){
    echo "<pre>";
    var_dump( $var );
    if( $is_exit ){
        exit();
    }
}

/**
 * ajax公共返回数据拼接
 * @param int $status
 * @param array/str $content
 * @param str $msg
 * @return array
 */
function ajax_return_join($status,$content = array(),$msg){
	$data['status']  = $status;
	$data['content'] = $content;
	$data['msg'] = L($msg);
	return $data;
}

/**
 * [date2week 日期对应的周几]
 * @param  [type] $timestamp [时间戳]
 * @param  [type] $lang [返回周格式，zh-cn:周x，en-us : Mon～Sun]
 * @return [string|false]       [description]
 */
function date2week( $timestamp , $lang = null )
{
    $default_lang = array('zh-cn','en-us');
    if ( $lang==null|| !in_array($lang, $default_lang) )
    {
        $lang = cookie('think_language')?cookie('think_language'):"zh-cn";
    }

    if( $lang =="zh-cn" )
    {
        $cn_week = array(1=>"周一",2=>"周二",3=>"周三",4=>"周四",5=>"周五",6=>"周六",7=>"周天");
        return $cn_week[ date('N',$timestamp) ];
    }
    else if( $lang == 'en-us')
    {
        return date("D",$timestamp);
    }else{
        return false;
    }
}

/**
 *截取字符串
 * @param String $str
 * @param number $len
 * @param boolean $sub 是否已经截取
 * @param string $more
 * @return unknown|string
 */
function k_sub_str($str, $len = 17, $more = '...'){
	$strlen = mb_strlen($str, 'UTF-8');
	if($len >= $strlen)
	{
		return $str;
	}
	return mb_substr($str, 0, $len, 'UTF-8').$more;
}
/**
 * 状态显示
 * @param $state
 * @param $filed
 * @param $id
 */
function show_state($state){
	$s = array(0=>'<i class="fa txt-color-red fa-times"></i>',1=>'<i class="fa txt-color-green fa-check"></i>');
	return $s[$state];
}/**
 * 取得文件扩展
 *
 * @param $filename 文件名
 * @return 扩展名
 */
function fileext($filename) {
	return strtolower(trim(substr(strrchr($filename, '.'), 1, 10)));
}/**
 * 随机数函数
 * @param int,boolean $letter是否使用字母
 * @param int,boolean $numerical是否使用数字
 * @param int $lenth 返回长度
 */
function rand_str($letter=true, $numerical=true, $lenth=10){
	$let = $num = array();
	$chars = '';
	if(!$letter && !$numerical)return '';
	if($lenth <= 0)return '';
	if($letter)$let = range('a','z');
	if($numerical)$num = range(0,9);
	$array = array_merge($let,$num);
	$max = count($array) - 1;
	for($i = 0; $i < $lenth; $i++) {
		$chars .= $array[mt_rand(0, $max)];
	}
	return $chars;
}
/**
 * 编码输入
 */
function htmlspecialcharsEncode($input)
{
	if(is_array($input))
	{
		foreach ($input AS $k=>$v)
		{
			$input[$k] = htmlspecialcharsEncode($v);
		}
	}else{
		return htmlspecialchars($input, ENT_QUOTES);
	}
	return $input;
}
/**
 * 解码输出
 */
function htmlspecialcharsDecode($output)
{
	if(is_array($output))
	{
		foreach ($output AS $k=>$v)
		{
			$output[$k] = htmlspecialcharsDecode($v);
		}
	}else{
		return htmlspecialchars_decode($output, ENT_QUOTES);
	}
	return $output;
}


/**
 * 产生随机字符串
 *
 * @param    int        $length  输出长度
 * @param    string     $chars   可选的 ，默认为 0123456789
 * @return   string     字符串
 */
function random($length, $chars = '0123456789') {
    $hash = '';
    $max = strlen($chars) - 1;
    for($i = 0; $i < $length; $i++) {
        $hash .= $chars[mt_rand(0, $max)];
    }
    return $hash;
}
/**
 * [combos 组合数据]
 * @param  [type]  $data  [description]
 * @param  array   $all   [description]
 * @param  array   $group [description]
 * @param  [type]  $val   [description]
 * @param  integer $i     [description]
 * @return [type]         [description]
 */
function combos($data, &$all = array(), $group = array(), $val = null, $i = 0)
{
    if (isset($val))
    {
        array_push($group, $val);
    }
    if ($i >= count($data))
    {
        array_push($all, $group);
    }
    else
    {
        foreach ($data[$i] as $v)
        {
            combos($data, $all, $group, $v, $i + 1);
        }
    }

    return $all;
}
/**
 * 替换正式列表的数据
 */
function replaceOfficial($model, $id)
{
	if(!$id)return false;
	if(!$model)
	{
		$model = D('Apps');
	}
	$sql = "REPLACE INTO `k_apps_official` SELECT * FROM k_apps WHERE id={$id}";
	return $model -> execute($sql);
}
/**
 * 编辑记录器
 * @param  int $id 记录id
 * @param string $more 描述
 */
function edit_recorder($id, $more = '')
{
	$model = D('edit_recorder');

	$data['m'] = I("get.m");
	$data['c'] = CONTROLLER_NAME;
	$data['a'] = ACTION_NAME;
	$data['affectid'] = $id;
	$data['admin'] = session('username');
	if($more)
	{
		$data['more'] = $more;
	}
	$model -> data($data) ->add();
}


