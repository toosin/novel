<?php
// +----------------------------------------------------------------------
// | Qr控制器
// +----------------------------------------------------------------------
// | Since: 2016-7-8 12:23:00
// +----------------------------------------------------------------------
// | Author: shioujun <shioujun@gmail.com>
// +----------------------------------------------------------------------
namespace Crontab\Controller;

use Think\Controller;
use Common\Model\OpenIdModel as OpenId;
use Common\Model\UserModel as User;
use Com\Weixin\WeixinApi;
use Common\Model\QrModel as Qr;

/**
 */
class QrController extends Controller
{

    public function run()
    {
        $user = new User();
        $u_qr = D('qr');
        
        $run_time = 600;
        
        $start = time();
        while (true)
        {
            $where = array();
            $where['state'] = 1;
            $where['wxhao'] = C('WX_HAO');
            $uInfo = $u_qr->where($where)
                ->limit(60)
                ->select();
            
            foreach ($uInfo as $key => $value)
            {
                $where = array();
                $where['id'] = $value['userid'];
                $userInfo = $user->where($where)->find();
                
                $open_obj = new OpenId();
                $where = array();
                $where['uid'] = $value['userid'];
                $where['wxhao'] = C('WX_HAO');
                
                $open_info = $open_obj->where($where)->find();
                
                $media_id = $open_info['media_id'] ? $open_info['media_id'] : false;
                
                // 是否永久
                $media_limit = $open_info['media_limit'] == 1 ? true : false;
                
                // media id是否失效
                $media_is_expire = ((time() - $open_info['media_time']) > 3600 * 24 * 3) ? true : false;
                
                // 二维码是否失效
                if ($media_limit)
                {
                    $media_limit_is_expire = false;
                } else
                {
                    $media_limit_is_expire = ((time() - $open_info['media_time']) > 3600 * 24 * 30) ? true : false;
                }
                if ($media_id && ! $media_is_expire && ! $media_limit_is_expire)
                {
                    $weixin_obj = new WeixinApi();
                    $weixin_obj->sendPicture($open_info['openid'], $media_id);
                } else if ($userInfo)
                {
                    try
                    {
                        $qr_obj = new Qr();
                        
                        $qr_obj->qr($userInfo, $open_info['openid']);
                    } catch (\Exception $e)
                    {
                        my_log($e->getMessage());
                    }
                }
                
                $where = array();
                $where['id'] = $value['id'];
                $update = array();
                $update['state'] = 0;
                $u_qr->where($where)->save($update);
            }
            
            $now = time();
            
            if (($now - $start) > $run_time)
            {
                break;
            }
            
            if (empty($uInfo))
            {
                sleep(1);
            }
        }
    }

    
}