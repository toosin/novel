<?php
// +----------------------------------------------------------------------
// | 默认控制器
// +----------------------------------------------------------------------
// | Since: 2014-09-15 14:54
// +----------------------------------------------------------------------
// | Author: shioujun <shioujun@gmail.com>
// +----------------------------------------------------------------------

namespace Crontab\Controller;
use Think\Controller;

/**
 *Crontab项目默认控制器
 */
class IndexController extends Controller
{
    /**
     * [index 默认Action]
     * @return [type] [description]
     */
    public function index()
    {
        echo "default action:index\n";
    }
}