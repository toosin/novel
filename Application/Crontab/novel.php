<?php
// +----------------------------------------------------------------------
// | Crontab入口文件，只能在CLI下运行
// +----------------------------------------------------------------------
// | Since: 2014-09-15 14:54
// +----------------------------------------------------------------------
// | Author: shioujun <shioujun@gmail.com>
// +----------------------------------------------------------------------

// 检测PHP环境
if (version_compare(PHP_VERSION, '5.3.0', '<'))
    die('require PHP > 5.3.0 !');
    
    // 必须在CLI模式运行
if (substr(php_sapi_name(), 0, 3) !== 'cli') {
    die("This Programe can only be run in CLI mode\n");
}

// 改变目录
chdir(dirname(__FILE__));

define('BIND_MODULE', 'Crontab');
define('MODE_NAME', 'cli');
// 入口文件名
define('CURR_RUN_FILENAME', __FILE__);

define("APP_ROOT", dirname(dirname(dirname(__FILE__))));

// 定义应用目录
define('APP_PATH', '../../Application/');

// Runtime Path
define('RUNTIME_PATH', APP_PATH . 'Runtime_crontab_novel/');

define('APP_DEBUG', true);

// 线上环境使用online配置文件,个人环境请修改,注意不要将此修改提交SVN
define('APP_STATUS', 'novel');

// 引入ThinkPHP入口文件
require '../../ThinkPHP/ThinkPHP.php';

// 亲^_^ 后面不需要任何代码了 就是如此简单
