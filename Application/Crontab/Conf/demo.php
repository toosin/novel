<?php
/**
 * 线上配置文件，请不要轻易改动此文件
 * @author [shioujun] <[shioujun@gmail.com]>
 * @since 2014-08-08 10:27
 */
return array(    
    // 日志记录文件夹
    'CRONTAB_LOG_DIR' => '/tmp/demo/',
);