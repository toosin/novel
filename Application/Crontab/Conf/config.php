<?php
/**
 * 线上配置文件，请不要轻易改动此文件
 * @author [shioujun] <[shioujun@gmail.com]>
 * @since 2014-08-08 10:27
 */
return array(
    // PHP命令行程序路径
    'PHP_CLI_PATH' => '/usr/local/bin/php',
);