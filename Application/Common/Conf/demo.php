<?php
return array(
    // 数据库配置信息
    'DB_TYPE' => 'mysqli', // 数据库类型
    'DB_HOST' => '127.0.0.1', // 服务器地址
    'DB_NAME' => 'demo', // 数据库名
    'DB_USER' => 'root', // 用户名
    'DB_PWD' => '', // 密码
    'DB_PORT' => 3306, // 端口
    'DB_PREFIX' => 'bs_', // 数据库表前缀
    
    //默认MOUDLE
    'DEFAULT_MODULE' => 'Demo',
    //允许访问的MODULE
    'MODULE_ALLOW_LIST' => array(
        'Demo'
    ),
    
    //URL忽略大小写
    'URL_CASE_INSENSITIVE' => true,
    //REWRITE模式
    'URL_MODEL' => 2,
    
//     'MEMCACHE_HOST' => '127.0.0.1',
//     'MEMCACHE_PORT' => '11211',
    
//     'SESSION_TYPE' => 'Memcache',
    'SESSION_OPTIONS' => array(
        'name' => 'demo',
        'expire' => 1800
    ),
    
    'SESSION_PREFIX' => 'demo',
    'SESSION_DOMAIN' => "",
    
//     'DATA_CACHE_TYPE' => 'Memcache',
    'DATA_CACHE_PREFIX' => 'demo',
    
    
    /*-------一下为自定义配置-------*/
    
    'AppID' => 'test',
    'AppSecret' => 'test',
    'TOKEN' => "test",
    'EncodingAESKey' => 'test',
    //当前微信号的名称
    'WX_NAME' => '测试',
    //当前微信号的别名
    'WX_ALIAS' => '测试',
    //当前微信号，如果无，请填写微信名称的全拼
    'WX_HAO' => 'soj_kangzi',

    /**
     * 域名相关
     */
    //主域名
    'MAIN_DOMAIN' => "soj.demo.com",
    //CDN域名，如果没有CDN请填写主域名的
    'CDN_DOMAIN' => "soj.demo.com",
    //CDN的URL
    'CDN_URL' => 'http://soj.demo.com/',

    //当前服务器的唯一标示
    'CLUSTER_ID'=>'1',
    
    
    //微信支付相关配置（当使用微信支付的时候配置），具体参数的详细说明请查看微信商户平台的文档
    "QIYE_PAY_URL"=>"https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers",
    "HONGBAO_PAY_URL"=>"https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack",
    "MCH_ID"=>"1297808501",
    "API_KEY"=>"8c949f5f0671054f6920af7a565c3475",
    "CERT_PATH"=>"/Application/Fenxiao/Cert/demo/apiclient_cert.pem",
    "CERT_KEY_PATH"=>"/Application/Fenxiao/Cert/demo/apiclient_key.pem",
    'NOTIFY_URL'=>'http://soj.demo.com/service/weixinNotify',
    'REPORT_LEVENL'=>0,
    'CURL_PROXY_HOST'=>'0.0.0.0',
    'CURL_PROXY_HOST'=>0,
    'APP_NAME'=>'商品',
    'NICK_NAME'=>'商品',

    //搜索音乐的数量
    'MUSIC_NUMBER' => 5,
    
);