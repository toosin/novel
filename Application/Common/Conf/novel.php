<?php
/**
 * Created by toosin.
 * Date: 2017/8/19 0019
 */
return array(
    // 数据库配置信息
    'DB_TYPE' => 'mysqli', // 数据库类型
    'DB_HOST' => 'rm-2ze11bm754mqfdm95o.mysql.rds.aliyuncs.com', // 服务器地址
    'DB_NAME' => 'novel', // 数据库名
    'DB_USER' => 'lanbin', // 用户名
    'DB_PWD' => '4d6763bcf5879f18c0034bb2d97c30a9', // 密码
    'DB_PORT' => 3306, // 端口
    'DB_PREFIX' => 'bs_', // 数据库表前缀

    //默认MOUDLE
    'DEFAULT_MODULE' => 'Novel',
    //允许访问的MODULE
    'MODULE_ALLOW_LIST' => array(
        'Novel'
    ),

    //URL忽略大小写
    'URL_CASE_INSENSITIVE' => true,
    //REWRITE模式
    'URL_MODEL' => 2,

//     'MEMCACHE_HOST' => '127.0.0.1',
//     'MEMCACHE_PORT' => '11211',

//     'SESSION_TYPE' => 'Memcache',
     'SESSION_OPTIONS' => array(
        'name' => 'novel',
        'expire' => 1800
     ),

    'SESSION_PREFIX' => 'novel',
    'SESSION_DOMAIN' => "",

//    'DATA_CACHE_TYPE' => 'Memcache',
    'DATA_CACHE_PREFIX' => 'novel',
//备用:www.smswx.cn www.jczx114.com.cn www.etuina.cc
    'tzUrl'=>'www.swtx.org.cn',//跳转主域
    'apiUrl'=>'www.daxiaozi.cc',//业务主域
    'shareUrl'=>'www.aixiyi.cc',//分享主域

    'anonymous_name'=>'游客',
    'anonymous_logo'=>'http://wx.qlogo.cn/mmopen/w4FI6UPu6PWHtAfMWM2oQpzGrHSiaPicDwzsneicrDibqwAJ9QgGlURK5sR8Mvv6XPL2xRaXVlFIdqTfCtSVJl5xOPPiaV6eJdyWk/0',
);