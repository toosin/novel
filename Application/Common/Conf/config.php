<?php
return array(
	//'配置项'=>'配置值'
	
    'TMPL_ACTION_ERROR'     =>  APP_ROOT.'/Application/Common/View/Msg/dispatch_jump.html', // 默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS'   =>  APP_ROOT.'/Application/Common/View/Msg/dispatch_jump.html', // 默认成功跳转对应的模板文件
    'TMPL_EXCEPTION_FILE'   =>  APP_ROOT.'/Application/Common/View/Msg/exception.html',// 异常页面的模板文件
    'ERROR_MESSAGE'         =>  '页面错误，请稍等再试~',//错误显示信息,非调试模式有效
    'ERROR_PAGE'            =>  '', // 错误定向页面
);