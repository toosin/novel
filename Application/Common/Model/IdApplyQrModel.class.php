<?php
namespace Common\Model;

use Think\Model;

class IdApplyQrModel extends Model
{

    protected $tableName = 'id_qr';

    /**
     * 申请 id
     * 
     * @param unknown_type $type            
     * @return int id
     */
    public function apply($type = 'user')
    {
        $data = array();
        $data['type'] = $type;
        return $this->add($data);
    }
}