<?php
namespace Common\Controller;

use Think\Controller;

/**
 * QQ音乐接口
 * Class Music
 * @package Common\Controller
 */
class MusicController extends Controller{

    public function qq_music($search_music ='')
    {

        $music_name = $search_music ? $search_music :false;
        $page_size = C('MUSIC_NUMBER') ? C('MUSIC_NUMBER') : 5;
        $url = "http://s.music.qq.com/fcgi-bin/music_search_new_platform?t=0&n=%s&aggr=1&cr=1&loginUin=0&format=json&inCharset=GB2312&outCharset=utf-8&notice=0&platform=jqminiframe.json&needNewCode=0&p=1&catZhida=0&remoteplace=sizer.newclient.next_song&w=%s";
        $url = sprintf($url, $page_size, urlencode($music_name));
        $filedata = file_get_contents($url);
        $filedata = json_decode($filedata,true);

        $filedata = $filedata['data']['song']['list'];

        $arr_music_f = array();
        $mid = '';
        $data = array();

        foreach($filedata as  $key => $item){
            $data[$key]['music_name'][] = $item['fsong'];
            $arr_music_f = explode("|",$item['f']);
            $mid = $arr_music_f[0];
            $image_id = $arr_music_f[22];
            $data[$key]['images'][] = "http://y.gtimg.cn/music/photo_new/T002R300x300"."M000"."{$image_id}".".jpg";
            $str = file_get_contents("http://music.qq.com/miniportal/static/lyric/".($mid%100)."/{$mid}.xml");
            $data[$key]['song'][] = iconv('GB2312', 'UTF-8', $str);
            $data[$key]['music_url'][] = "http://stream.qqmusic.tc.qq.com/{$mid}.mp3";
        }

        return $data;exit();
    }

}